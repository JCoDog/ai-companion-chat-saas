/** @type {import('next').NextConfig} */
const nextConfig = {
	images: {
		domains: ["localhost", "res.cloudinary.com", "cdn.discordapp.com"],
	},
};

module.exports = nextConfig;
