const { PrismaClient } = require("@prisma/client")

const db = new PrismaClient()

async function main() {
    try {
        console.log("Seeding default categories...")
        await db.category.createMany({
            data: [
                { name: "Famous people" },
                { name: "Movies & TV" },
                { name: "Musicians" },
                { name: "Games" },
                { name: "Animals" },
                { name: "Philosophy" },
                { name: "Scientists" },
                { name: "Non-Famous people" },
            ]
        })
        console.log("Default categories seeded!")
    } catch (error) {
     console.error("Error seeding default categories", error)
    } finally {
        await db.$disconnect()
    }
}

main()