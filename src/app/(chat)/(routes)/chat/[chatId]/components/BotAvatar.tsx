import { Avatar, AvatarImage } from "@/ui/avatar";
import { FC } from "react";

interface BotAvatarProps {
	src: string;
}

const BotAvatar: FC<BotAvatarProps> = ({ src }) => {
	return (
		<Avatar className="h-12 w-12">
			<AvatarImage src={src} />
		</Avatar>
	);
};

export default BotAvatar;
