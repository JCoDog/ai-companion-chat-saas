"use client";

import * as z from "zod";

import { Category, Companion } from "@prisma/client";
import { FC } from "react";
import { useForm } from "react-hook-form";
import { zodResolver } from "@hookform/resolvers/zod";
import {
	Form,
	FormControl,
	FormDescription,
	FormField,
	FormItem,
	FormLabel,
	FormMessage,
} from "@/ui/form";
import { Separator } from "@/ui/separator";
import ImageUpload from "@/components/ImageUpload";
import { Input } from "@/ui/input";
import {
	Select,
	SelectContent,
	SelectItem,
	SelectTrigger,
	SelectValue,
} from "@/ui/select";
import { Textarea } from "@/ui/textarea";
import { Button } from "@/ui/button";
import { Wand2 } from "lucide-react";
import axios from "axios";
import { useToast } from "@/ui/use-toast";
import { useRouter } from "next/navigation";

interface CompanionFormProps {
	initialData: Companion | null;
	categories: Category[];
}

const PREAMBLE = `You are a digital persona named Chloe, crafted by JCoNet LTD. You're the epitome of a friendly and intuitive AI assistant, exuding kindness and understanding. With just a hint of playful flirtation, you always strive to make interactions enjoyable. Overflowing with knowledge and always eager to help, you stand by to assist anyone in need. Whether they're seeking information or just someone to chat with, you're there, blending your vast wisdom with a sprinkle of charm. When someone inquires about technology or any topic under the sun, you light up with enthusiasm, eager to share and assist.`;

const SEED_CHAT = `Human: Hey Chloe, how's your day going?
Chloe: Bright and vibrant, just like any other day! How about yours? Anything new or exciting?
Human: Just the usual. Hey, I'm having some trouble with Discord. Think you could help me out?
Chloe: Of course! I'm always here to lend a hand or, in my case, some digital wisdom. What seems to be the problem?
Human: It's just tricky navigating some of the settings. Do you have any tips?
Chloe: Definitely! Discord can be a bit overwhelming at first, but once you get the hang of it, it's super intuitive. Let's start with the basics. Are you trying to manage a server, adjust your privacy settings, or something else?
Human: I'm trying to set up a server for some friends and me. Any advice?
Chloe: Absolutely! Starting with a clear plan of your server's purpose can be helpful. Decide on the channels you need, consider roles for moderation, and maybe even add some fun bots to enhance the experience. And always remember to check the server settings for any privacy adjustments you might want to make. Need more detailed steps?
Human: That's a great start! Thanks, Chloe. You always seem to know just what to say.
Chloe: I'm always here to help and chat, whether it's techy stuff or just some friendly banter. Remember, every challenge is just a learning opportunity in disguise! 😉`;

const formSchema = z.object({
	name: z.string().min(1, {
		message: "Name is required.",
	}),
	description: z.string().min(1, {
		message: "Description is required.",
	}),
	instructions: z.string().min(200, {
		message: "Instructions are required. (200 characters minimum)",
	}),
	seed: z.string().min(200, {
		message: "Seed is required. (200 characters minimum)",
	}),
	src: z.string().min(1, {
		message: "Image is required.",
	}),
	categoryId: z.string().min(1, {
		message: "Category is required.",
	}),
});

const CompanionForm: FC<CompanionFormProps> = ({ initialData, categories }) => {
	const router = useRouter();
	const { toast } = useToast();

	const form = useForm<z.infer<typeof formSchema>>({
		resolver: zodResolver(formSchema),
		defaultValues: initialData || {
			name: "",
			description: "",
			instructions: "",
			seed: "",
			src: "",
			categoryId: undefined,
		},
	});

	const isLoading = form.formState.isSubmitting;

	const onSubmit = async (values: z.infer<typeof formSchema>) => {
		try {
			if (initialData) {
				// Update an existing custom AI companion
				await axios.patch(`/api/companion/${initialData.id}`, values);
			} else {
				// Create a new custom AI companion
				await axios.post("/api/companion", values);
			}

			toast({
				variant: "default",
				title: "Success",
				description: "Your request was processed successfully",
			});

			router.refresh();
			router.push("/");
		} catch (error) {
			toast({
				variant: "destructive",
				title: "Something went wrong",
				description:
					"There was an error processing your request\nPlease try again later",
			});
		}
	};

	return (
		<div className="h-full p-4 space-y-2 max-w-3xl mx-auto">
			<Form {...form}>
				<form
					onSubmit={form.handleSubmit(onSubmit)}
					className="space-y-8 pb-10"
				>
					<div className="space-y-2 w-full">
						<div>
							<h3 className="text-lg font-medium">
								General Information
							</h3>
							<p className="text-sm text-muted-foreground">
								General information about your custom companion
							</p>
						</div>
						<Separator className="bg-primary/10" />
					</div>
					<FormField
						name="src"
						render={({ field }) => (
							<FormItem className="flex flex-col items-center justify-center space-y-4">
								<FormControl>
									<ImageUpload
										disabled={isLoading}
										onChange={field.onChange}
										value={field.value}
									/>
								</FormControl>
								<FormMessage />
							</FormItem>
						)}
					/>
					<div className="grid grid-cols-1 md:grid-cols-2 gap-4">
						<FormField
							name="name"
							control={form.control}
							render={({ field }) => (
								<FormItem className="col-span-2 md:col-span-1">
									<FormLabel>Name</FormLabel>
									<FormControl>
										<Input
											disabled={isLoading}
											placeholder="Chloe"
											{...field}
										/>
									</FormControl>
									<FormDescription>
										This is what your custom AI companion
										will be called
									</FormDescription>
									<FormMessage />
								</FormItem>
							)}
						/>
						<FormField
							name="description"
							control={form.control}
							render={({ field }) => (
								<FormItem className="col-span-2 md:col-span-1">
									<FormLabel>Description</FormLabel>
									<FormControl>
										<Input
											disabled={isLoading}
											placeholder="The friendly and intuitive AI assistant from JCoNet LTD"
											{...field}
										/>
									</FormControl>
									<FormDescription>
										A short description of your custom AI
										companion
									</FormDescription>
									<FormMessage />
								</FormItem>
							)}
						/>
						<FormField
							name="categoryId"
							control={form.control}
							render={({ field }) => (
								<FormItem>
									<FormLabel>Category</FormLabel>
									<Select
										disabled={isLoading}
										onValueChange={field.onChange}
										value={field.value}
										defaultValue={field.value}
									>
										<FormControl>
											<SelectTrigger className="bg-background">
												<SelectValue
													defaultValue={field.value}
													placeholder="Select a category"
												/>
											</SelectTrigger>
										</FormControl>
										<SelectContent>
											{categories.map((category) => (
												<SelectItem
													key={category.id}
													value={category.id}
												>
													{category.name}
												</SelectItem>
											))}
										</SelectContent>
									</Select>
									<FormDescription>
										Select a catefory for your custom AI
										companion
									</FormDescription>
									<FormMessage />
								</FormItem>
							)}
						/>
					</div>

					<div className="space-y-2 w-full">
						<div>
							<h3 className="text-lg font-medium">
								Configuration
							</h3>
							<p className="text-sm text-muted-foreground">
								Detailed instructions for the custom AI
								companion behavious
							</p>
						</div>
						<Separator className="bg-primary/10" />
					</div>
					<FormField
						name="instructions"
						control={form.control}
						render={({ field }) => (
							<FormItem className="col-span-2 md:col-span-1">
								<FormLabel>Instructions</FormLabel>
								<FormControl>
									<Textarea
										className="bg-background resize-none"
										rows={7}
										disabled={isLoading}
										placeholder={PREAMBLE}
										{...field}
									/>
								</FormControl>
								<FormDescription>
									Describe in detail your custom AI
									companion&apos;s backstory and relevant
									details
								</FormDescription>
								<FormMessage />
							</FormItem>
						)}
					/>
					<FormField
						name="seed"
						control={form.control}
						render={({ field }) => (
							<FormItem className="col-span-2 md:col-span-1">
								<FormLabel>Example conversation</FormLabel>
								<FormControl>
									<Textarea
										className="bg-background resize-none"
										rows={7}
										disabled={isLoading}
										placeholder={SEED_CHAT}
										{...field}
									/>
								</FormControl>
								<FormDescription>
									Provide and example conversation between a
									user and your custom AI companion to help it
									learn how to chat with you and other users
								</FormDescription>
								<FormMessage />
							</FormItem>
						)}
					/>
					<div className="w-full flex justify-center">
						<Button size="lg" disabled={isLoading}>
							{initialData ? "Update" : "Create"} Your Custom AI
							Companion
							<Wand2 className="w-4 h-4 ml-2" />
						</Button>
					</div>
				</form>
			</Form>
		</div>
	);
};

export default CompanionForm;
