import prismadb from "@/lib/prismadb";
import { FC } from "react";
import CompanionForm from "./componenets/CompanionForm";
import { auth, redirectToSignIn } from "@clerk/nextjs";

interface pageProps {
	params: {
		companionId: string;
	};
}

const page: FC<pageProps> = async ({ params: { companionId } }) => {
	const { userId } = auth();

	if (!userId) {
		return redirectToSignIn();
	}

	// ToDo: Check subscription

	const companion = await prismadb.companion.findUnique({
		where: {
			id: companionId,
			userId,
		},
	});

	const categories = await prismadb.category.findMany();

	return <CompanionForm initialData={companion} categories={categories} />;
};

export default page;
