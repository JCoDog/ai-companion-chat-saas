import {
	Card,
	CardContent,
	CardFooter,
	CardHeader,
} from "@/components/ui/card";
import { Separator } from "@/components/ui/separator";
import { checkSubscription } from "@/lib/subscription";

const page = async () => {
	const isPro = await checkSubscription();

	return (
		<div className="h-full p-4 space-y-2">
			<h3 className="text-lg font-medium">Features</h3>
			<div className="text-muted-foreground text-sm">
				{isPro
					? "You are subscribed to the Pro plan."
					: "You are currently on the Free plan."}
			</div>

			<div className="grid grid-cols-2 sm:grid-cols-3 md:grid-cols-4 lg:grid-cols-5 xl:grid-cols-6 gap-2 pb-10">
				{/* free plan */}
				<Card
					key="Free plan"
					className="bg-primary/10 rounded-cl cursor-default border-0 p-3"
				>
					<CardHeader className="flex items-center justify-center text-center">
						<p className="font-bold">Free plan</p>
						<p className="text-xs">
							Explore the Companion Chat site
						</p>
					</CardHeader>
					<Separator className="mb-3 bg-black dark:bg-white" />
					<CardContent className="flex items-center justify-center text-center">
						<ul className="list-disc list-inside">
							<li>Access to already made companions</li>
							<li>Up to 100 messages a month</li>
						</ul>
					</CardContent>
				</Card>
				{/* pro plan */}
				<Card
					key="Free plan"
					className="bg-primary/10 rounded-cl cursor-default border-0 p-3"
				>
					<CardHeader className="flex items-center justify-center text-center">
						<p className="font-bold">Pro plan</p>
						<p className="text-xs">
							Unlock all the features of the Companion Chat site
						</p>
					</CardHeader>
					<Separator className="mb-3 bg-black dark:bg-white" />
					<CardContent className="flex items-center justify-center text-center">
						<ul className="list-disc list-inside">
							<li>Access to already made companions</li>
							<li>Create and edit your own custom companions</li>
							<li>Unlimited messages</li>
						</ul>
					</CardContent>
					<CardFooter className="flex items-center justify-between text-xs">
						<p className="text-2xl font-medium">
							£10
							<span className="text-sm font-normal">
								.99 / month
							</span>
						</p>
					</CardFooter>
				</Card>
			</div>
		</div>
	);
};

export default page;
