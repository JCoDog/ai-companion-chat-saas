import { FC } from "react";
import Categories from "@/components/Categories";
import SearchInput from "@/components/SearchInput";
import prismadb from "@/lib/prismadb";
import Companions from "@/components/Companions";

interface PageProps {
	searchParams: {
		categoryId: string;
		name: string;
	};
}

const page: FC<PageProps> = async ({ searchParams }) => {
	const data = await prismadb.companion.findMany({
		where: {
			categoryId: searchParams.categoryId,
			name: {
				search: searchParams.name,
			},
		},
		orderBy: {
			createdAt: "asc",
		},
		include: {
			_count: {
				select: {
					messages: true,
				},
			},
		},
	});

	const categories = await prismadb.category.findMany();

	return (
		<div className="h-full p-4 space-y-2">
			<SearchInput />
			<Categories data={categories} />
			<Companions data={data} />
		</div>
	);
};

export default page;
