import { ThemeProvider } from "@/components/ThemeProvider";
import { Toaster } from "@/ui/toaster";
import { cn } from "@/lib/utils";
import "@/styles/globals.css";
import { ClerkProvider } from "@clerk/nextjs";
import type { Metadata } from "next";
import { Inter } from "next/font/google";
import ProModal from "@/components/ProModal";

const inter = Inter({ subsets: ["latin"] });

export const metadata: Metadata = {
	title: "JCoNet AI",
	description: "Your unique AI Solutions provider by JCoNet LTD",
};

export default function RootLayout({
	children,
}: {
	children: React.ReactNode;
}) {
	return (
		<ClerkProvider>
			<html lang="en" suppressHydrationWarning>
				<body className={cn("bg-secondary", inter.className)}>
					<ThemeProvider
						attribute="class"
						defaultTheme="system"
						enableSystem
					>
						<ProModal />
						{children}
						<Toaster />
					</ThemeProvider>
				</body>
			</html>
		</ClerkProvider>
	);
}
