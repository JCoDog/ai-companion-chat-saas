"use client";

import { cn } from "@/lib/utils";
import { Sparkles } from "lucide-react";
import { Poppins } from "next/font/google";
import Link from "next/link";
import { UserButton } from "@clerk/nextjs";
import { Button } from "@/ui/button";
import { ThemeToggle } from "@/components/ThemeToggle";
import { useTheme } from "next-themes";
import { dark } from "@clerk/themes";
import MobileSidebar from "@/components/MobileSidebar";
import { useProModal } from "@/hooks/useProModal";

const font = Poppins({
	weight: "600",
	subsets: ["latin"],
});

interface NavbarProps {
	isPro: boolean;
}

const NavBar = ({ isPro }: NavbarProps) => {
	const { resolvedTheme } = useTheme();
	const proModal = useProModal();

	return (
		<div className="fixed w-full z-50 flex justify-between items-center py-2 px-4 border-b border-primary/10 bg-secondary h-16">
			<div className="flex items-center">
				<MobileSidebar isPro={isPro} />
				<Link href="/">
					<h1
						className={cn(
							"hidden md:block text-xl md:text-3xl font-bold text-primary",
							font.className
						)}
					>
						AI Companions
					</h1>
				</Link>
			</div>

			<div className="flex items-center gap-x-3">
				{!isPro && (
					<Button
						onClick={proModal.onOpen}
						variant="premium"
						size="sm"
					>
						Upgrade
						<Sparkles className="h-4 w-4 fill-white text-white ml-2" />
					</Button>
				)}
				<ThemeToggle />
				<UserButton
					appearance={{
						baseTheme: resolvedTheme === "dark" ? dark : undefined,
					}}
					afterSignOutUrl="/"
				/>
			</div>
		</div>
	);
};

export default NavBar;
