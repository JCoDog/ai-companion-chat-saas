"use client";

import { FC, useEffect, useState } from "react";
import {
	Dialog,
	DialogContent,
	DialogDescription,
	DialogHeader,
	DialogTitle,
} from "@/ui/dialog";
import { useProModal } from "@/hooks/useProModal";
import { Separator } from "@/ui/separator";
import { Button } from "@/ui/button";
import { useToast } from "@/ui/use-toast";
import axios from "axios";

interface ProModalProps {}

const ProModal: FC<ProModalProps> = ({}) => {
	const [isMounted, setIsMounted] = useState(false);

	useEffect(() => {
		setIsMounted(true);
	}, []);

	const proModal = useProModal();
	const { toast } = useToast();
	const [loading, setLoading] = useState(false);

	const onSubscribe = async () => {
		try {
			setLoading(true);
			const response = await axios.get("/api/stripe");

			window.location.href = response.data.url;
		} catch (error) {
			toast({
				title: "Error Subscribing",
				description: "Something went wrong. Please try again later.",
				variant: "destructive",
			});
		} finally {
			setLoading(false);
		}
	};

	if (!isMounted) return null;

	return (
		<Dialog open={proModal.isOpen} onOpenChange={proModal.onClose}>
			<DialogContent>
				<DialogHeader className="space-y-4">
					<DialogTitle className="text-center">
						Upgrade to Pro
					</DialogTitle>
					<DialogDescription className="text-center space-y-2">
						Upgrade to pro to unlock
						<span className="text-sky-500 mx-1 font-medium">
							all features
						</span>
						<Separator />
						<ul className="list-disc list-inside">
							<li>Access to already made companions</li>
							<li>Create and edit your own custom companions</li>
							<li>Unlimited messages</li>
						</ul>
					</DialogDescription>
				</DialogHeader>
				<Separator />
				<div className="flex justify-between">
					<p className="text-2xl font-medium">
						£10
						<span className="text-sm font-normal">.99 / month</span>
					</p>
					<Button
						onClick={onSubscribe}
						variant="premium"
						disabled={loading}
					>
						Subscribe
					</Button>
				</div>
			</DialogContent>
		</Dialog>
	);
};

export default ProModal;
